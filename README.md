# [Delivery Auto](http://www.delivery-auto.com/ "Delivery Auto") API PHP Client

> *Currently in development. Things may change or break until a solid release has been announced.*

## Requirements

* PHP 5.4 and later
* cURL PHP extension

#
## About

This client covers all methods of [Delivery Auto](http://www.delivery-auto.com/ "Delivery Auto") **API v4**.
API is completely open. It means that you don't need any login, password, key to work with it.

## Available methods

#### Agencies

* [Get list of regions](../master/docs/agencies/get-list-of-regions.md)
* [Get list of areas (cities)](../master/docs/agencies/get-list-of-ares.md)
* [Get list of warehouses](../master/docs/agencies/get-list-of-warehouses.md)
* [Get warehouses info](../master/docs/agencies/get-warehouse-info.md)
* [Find warehouses](../master/docs/agencies/find-warehouses.md)
* [Get list of warehouses in detail](../master/docs/agencies/get-list-of-warehouses-in-detail.md)

#### Receipts

* [Get receipt details](../master/docs/receipts/get-receipts-details.md)
* [Get arrival date](../master/docs/receipts/get-arrival-date.md)

#### Cost

* [Get list of additional services](../master/docs/cost/get-list-of-additional-services.md)
* [Get list of traffic categories](../master/docs/cost/get-list-of-traffic-categories.md)
* [Get list of delivery schemas](../master/docs/cost/get-list-of-delivery-schemas.md)
* [Calculate fare](../master/docs/cost/calculate-fare.md)

# Using

```php
<?php

namespace Acme;

use Amass\DeliveryAuto\Client;

class Foo
{
    public function bar($text)
    {
        $client = new Client();
        
        // Get raw response
        $result = $client->getListOfRegions()->getRawResponse();        
        // Get raw array result
        $result = $client->getListOfRegions()->getRawArrayResult();
        // Get object mapped result
        $result = $client->getListOfRegions()->getObjectMappedResult();
    }
}
```
