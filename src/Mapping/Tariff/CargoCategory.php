<?php
/*
 * This file is part of the "Delivery Auto" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\DeliveryAuto\Mapping\Tariff;

/**
 * Cargo Category Entity Mapping Class
 *
 * @author Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 */
class CargoCategory
{
    /**
     * @var string $id ID
     */
    private $id;

    /**
     * @var string $name Name
     */
    private $name;

    /**
     * Set ID
     *
     * @param string $id ID
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get ID
     *
     * @return string ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name Name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string Name
     */
    public function getName()
    {
        return $this->name;
    }
}