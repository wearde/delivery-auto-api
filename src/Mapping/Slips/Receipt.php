<?php

/*
 * This file is part of the "Delivery Auto" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\DeliveryAuto\Mapping\Slips;
/**
 * Service Entity Mapping Class
 *
 * @author Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 */

class Receipt
{
    /**
     * @var int $number ID
     */
    private $number;

    /**
     * @var float $cost Cost
     */
    private $cost;

    /**
     * @var array $currencyCode egs []
     */
    private $egs = [];

    /**
     * Set Number
     *
     * @param int $number Number
     *
     * @return $this Service
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get Number
     *
     * int $number Number
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set cost
     *
     * @param float $cost Cost
     *
     * @return $this Service
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return float Cost
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set Egs
     *
     * @param array $egs Currency code
     *
     * @return $this Service
     */
    public function setEgs($egs)
    {
        $this->egs = $egs;

        return $this;
    }

    /**
     * Get Egs
     *
     * @return array egs
     */
    public function getEgs()
    {
        return $this->egs;
    }
}