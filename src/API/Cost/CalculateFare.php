<?php
/*
 * This file is part of the "Delivery Auto" API PHP Client
 *
 * (c) Amass Advance <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\DeliveryAuto\API\Cost;

use Amass\DeliveryAuto\API\AbstractApiMethod;
use Amass\DeliveryAuto\Mapping\Delivery\Schema;

/**
 * API method to calculate fare
 *
 * @author Amass Advance <wearde.studio@gmail.com>
 */
class CalculateFare extends AbstractApiMethod
{
    /**
     * {@inheritdoc}
     */
    protected static $partOfUrl = 'Public/PostReceiptCalculate';

    protected $data;
    /**
     * Constructor
     *
     * @param array $data POST params
     */
    public function __construct($data)
    {
        parent::__construct();

        $this->setHeader('headers/HMACAuthorization', '');

        $this->data = $data;
    }

    /**
     * Get raw response
     *
     * @return mixed Raw response
     */
    public function getRawResponse()
    {
        return $this->getGuzzleClient()->post($this->buildMethodApiUrl(array()), [ 'body' => $this->data ])->json();
    }

    /**
     * Get object mapped result
     *
     * @return array|Schema[]
     */
    public function getObjectMappedResult()
    {
        $result = [];

        return $result;
    }

    /**
     * Build API Method URL
     *
     * @param array $queryParams Query params
     * @return string API Method URL
     */
    protected function buildMethodApiUrl(array $queryParams )
    {
        return static::$apiUrl . static::$partOfUrl;
    }
}
