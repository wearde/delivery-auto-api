<?php
/*
 * This file is part of the "Delivery Auto" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\DeliveryAuto\API\Cost;

use Amass\DeliveryAuto\API\AbstractApiMethod;
use Amass\DeliveryAuto\Mapping\Tariff\CargoCategory;

/**
 * API method to get list of tariff cargo categories
 *
 * @author Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 */
class ListOfCargoCategory extends AbstractApiMethod
{
    /**
     * @var string $apiUrl apiUrl API URL
     * @static
     */
    protected static $apiUrl = 'http://www.delivery-auto.com/api/v4/';
    /**
     * {@inheritdoc}
     */
    protected static $partOfUrl = 'Public/GetCargoCategory';

    /**
     * Constructor
     *
     * @param string $locale Locale
     * @param string $tariffCategoryId Id fare categories
     */
    public function __construct($locale, $tariffCategoryId)
    {
        parent::__construct();

        $this->queryParams = [
            'culture'      => $locale,
            'TariffCategoryId' => $tariffCategoryId
        ];
    }

    /**
     * Get object mapped result
     *
     * @return array|$cargoCategory[]
     */
    public function getObjectMappedResult()
    {
        $result = [];

        foreach ($this->getArrayResult() as $item) {
            $id         = isset($item['id']) ? $item['id'] : null;
            $name       = isset($item['name']) ? $item['name'] : null;

            $cargoCategory = (new CargoCategory())->setId($id)
                ->setName($name);

            array_push($result, $cargoCategory);
        }

        return $result;
    }

}