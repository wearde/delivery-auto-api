<?php
/*
 * This file is part of the "Delivery Auto" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\DeliveryAuto\API;

use GuzzleHttp\Client as GuzzleClient;

/**
 * Abstract API Method Class
 *
 * @author Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * @abstract
 */
abstract class AbstractApiMethod
{
    /**
     * @var string $apiUrl apiUrl API URL
     * @static
     */
    protected static $apiUrl = 'http://www.delivery-auto.com/api/v2/';

    /**
     * @var string $partOfUrl Part of URL
     * @static
     */
    protected static $partOfUrl = '';

    /**
     * @var array $queryParams Query params
     */
    protected $queryParams = [];

    /**
     * @var GuzzleClient $guzzleClient Guzzle HTTP client
     */
    protected $guzzleClient;

    protected $headers;

    /**
     * Method returns object mapped result depends on each API call
     *
     * @abstract
     */
    abstract public function getObjectMappedResult();

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->guzzleClient = new GuzzleClient();
    }

    protected function setHeader($key, $value)
    {
        $this->guzzleClient->setDefaultOption($key, $value);
    }



    /**
     * Get raw response
     *
     * @return mixed Raw response
     */
    public function getRawResponse()
    {
        return $this->getGuzzleClient()->get($this->buildMethodApiUrl($this->queryParams))->json();
    }

    /**
     * Get array result
     *
     * @throws \Exception
     *
     * @return mixed Raw array result
     */
    public function getArrayResult()
    {
        $rawResponse = $this->getRawResponse();

        if (!empty($rawResponse) && is_array($rawResponse)) {
            if ($rawResponse['status']) {
                return $rawResponse['data'];
            }

           return false;// throw new \Exception($rawResponse['message']);
        }

        throw new \Exception('Wrong response type');
    }

    /**
     * Get Guzzle client
     *
     * @return GuzzleClient Guzzle client
     */
    protected function getGuzzleClient()
    {
        return $this->guzzleClient;
    }

    /**
     * Build API Method URL
     *
     * @param array $queryParams Query params
     *
     * @return string API Method URL
     */
    protected function buildMethodApiUrl(array $queryParams)
    {
        return static::$apiUrl . static::$partOfUrl . '?' . http_build_query($queryParams);
    }
}
