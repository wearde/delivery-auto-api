<?php

/**
 * @author Amass Advance <wearde.studio@gmail.com>
 * @link http://amass.pp.ua
 */
namespace Amass\DeliveryAuto\API\Slips;

use Amass\DeliveryAuto\API\AbstractApiMethod;
use Amass\DeliveryAuto\Mapping\Delivery\Schema;
use Amass\DeliveryAuto\Mapping\Slips\Receipt;

class PostCreateReceipts extends AbstractApiMethod
{
    /**
     * @var string $apiUrl apiUrl API URL
     * @static
     */
    protected static $apiUrl = 'http://www.delivery-auto.com/api/v4/';
    /**
     * {@inheritdoc}
     */
    protected static $partOfUrl = 'Public/PostCreateReceipts';

    /**
     * {@inheritdoc}
     */
    protected $data;

    /**
     *  HMACAuthorization Method 6.1 from API documentation
     */
    private $_hmac;
    /**
     * Constructor
     *
     * @param string $HMACAuthorization HMACAuthorization Method 6.1 from API documentation
     * @param array $data input Data
     */
    public function __construct($HMACAuthorization, $data)
    {
        parent::__construct();
        $this->_hmac = $HMACAuthorization;
        $this->data = $data;

    }

    /**
     * Get object mapped result
     *
     * @return array|Receipt[]
     */
    public function getObjectMappedResult()
    {
        $result = [];
        $response = $this->getCurl();

        if(empty($response->status))
            return false;

        foreach ($response->receipts as $receipt) {
            $number            = isset($receipt->Number) ?  $receipt->Number : null;
            $cost              = isset($receipt->TotallCost) ? $receipt->TotallCost : null;
            $egs = array();
            foreach($receipt->egs as $eg)
            {
                $egs[] = (array)$eg;
            }
            $category = (new Receipt())
                ->setNumber($number)
                ->setCost($cost)
                ->setEgs($egs);

            array_push($result, $category);
        }
        return $result;
    }

    /**
     * Get raw response @todo поки є якась не зрозуміла делема то поверннення через curl не працює Guzzle (може звязано з версіями хз)
     *
     * @return mixed Raw response
     */
    public function getCurl()
    {
        try{
            $ch = curl_init();
            if (false === $ch){
                throw new \Exception('failed to initialize');
            }
            curl_setopt($ch, CURLOPT_URL, static::$apiUrl . static::$partOfUrl);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->data));  //Json Post
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $headers = array();
            $headers[] = 'HMACAuthorization: ' . $this->_hmac;
            $headers[] = 'Content-Type: application/json; charset=utf-8';

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $server_output = curl_exec($ch);

            if (false === $server_output) {
                throw new \Exception(curl_error($ch), curl_errno($ch));
            } else{
                curl_close($ch);
                return json_decode($server_output);
            }
        }catch(\Exception $e) {
            trigger_error(sprintf('Curl failed with error #%d: %s',$e->getCode(), $e->getMessage()),E_USER_ERROR);
        }
    }
}