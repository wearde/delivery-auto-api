<?php
/*
 * This file is part of the "Meest Express" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\DeliveryAuto\Directory;

/**
 * Locale
 *
 * @author Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 */
class Locale
{
    const UKRAINIAN = 'uk-UA';

    const ENGLISH = 'en-US';

    const RUSSIAN = 'ru-RU';
}
