<?php
/*
 * This file is part of the "Delivery Auto" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\DeliveryAuto\Directory;

/**
 * Currency
 *
 * @author Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 */
class Currency
{
    const UAH = 100000000;

    const RUB = 100000001;
}
